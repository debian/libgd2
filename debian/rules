#!/usr/bin/make -f
# -*- makefile -*-
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
# DH_VERBOSE = 1

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
# package maintainers to append LDFLAGS
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# see EXAMPLES in dpkg-buildflags(1) and read /usr/share/dpkg/*
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

export DPKG_GENSYMBOLS_CHECK_LEVEL=4

DEB_CODENAME := $(shell sh -c '. /etc/os-release; echo "$${VERSION_CODENAME}"')
ifeq ($(DEB_CODENAME),$(filter $(DEB_CODENAME),buster bionic focal))
CONFIGURE_AVIF := --without-avif
CONFIGURE_HEIF := --without-heif
else
CONFIGURE_AVIF := --with-avif=/usr
CONFIGURE_HEIF := --with-heif=/usr
endif

%:
	dh $@ --with autoreconf

override_dh_autoreconf:
	dh_autoreconf --as-needed

override_dh_auto_configure:
	dh_auto_configure -- \
		--disable-rpath \
		--with-tiff=/usr \
		$(CONFIGURE_AVIF) \
		$(CONFIGURE_HEIF) \
		--libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH)

override_dh_install:
	dh_install --fail-missing -Xlibgd.la -Xgdlib-config

override_dh_strip:
	dh_strip -O--dbgsym-migration='libgd-dbg (<< 2.2.3-87-gd0fec80-1)'

override_dh_auto_test:
